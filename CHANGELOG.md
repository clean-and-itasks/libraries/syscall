# Changelog

#### 1.0.3

- Chore: support base `3.0`.

#### 1.0.2

- Fix: default implementation of `appWorld`.

#### 1.0.1

- Enhancement: Allow the function arguments for accWorld and appWorld to be unique also.

## 1.0.0

- Initial version
