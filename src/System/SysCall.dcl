definition module System.SysCall

/**
 * This module only contains a type class for environments that can perform
 * syscalls.
 *
 * If your unique environment contains the world (e.g. iTasks {{IWorld}}) you
 * can implement {{SysCallEnv}} safely. In other cases (e.g. database
 * transactions, cursors) you can use {{accUnsafe}} from {{System._Unsafe}} to
 * provide an instance for this type class.
 */

/**
 * The type class of environments that can perform syscalls.
 *
 * @var env the type of the environment
 */
class SysCallEnv env where
	/**
	 * Perform a function from {{World}} to {{World}} that produces a result
	 * using the environment.
	 *
	 * @param stateful function producing a result
	 * @param environment
	 * @param result
	 * @param new environment
	 */
	accWorld :: !.(*World -> *(.a, *World)) !*env -> *(!.a, !*env)
	/**
	 * Perform a function from {{World}} to {{World}} using the environment.
	 *
	 * @param stateful function
	 * @param environment
	 * @result new environment
	 */
	appWorld :: !.(*World -> *World) !*env -> *env
	appWorld f st = (\(_, env) = env) (accWorld (\w = ((), f w)) st)

instance SysCallEnv World
