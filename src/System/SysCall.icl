implementation module System.SysCall

instance SysCallEnv World where
	accWorld :: !.(*World -> *(.a, *World)) !*World -> *(!.a, !*World)
	accWorld f w = f w
