# syscalls

This library provides a unified interface for environments that are allowed to
do syscalls. The main type class is used in `linux`, `posix`, and `windows` to
constrain the environments with which syscalls can be performed.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
